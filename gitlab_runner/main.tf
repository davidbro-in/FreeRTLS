provider "aws" {
  region = "eu-west-1"
}

resource "aws_s3_bucket" "tfstate_bucket" {
  bucket = "my-tfstate-bucket"
  acl    = "private"
  region = "eu-west-1"
}