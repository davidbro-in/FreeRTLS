## Develpment
### Local

This repository can be used with [Visual Studio Code Dev Containers](
https://code.visualstudio.com/docs/devcontainers/containers). The official
Docker image `hashicorp/terraform` is used.

In order to use this repository:
1. Clone the main repository (if not done yet)
   ```sh
   git clone https://gitlab.com/davidbro-in/FreeRTLS.git
   ```
1. Login usign GitLab CLI
   ```sh
   glab auth login
   ```
1. Export the login variables for terraform API access to store state
   ```sh
   export GITLAB_PROJECT=45786056
   export GITLAB_TOKEN=$(echo '{ "name":"token", "scopes":["api"], "expires_at":"'$(date '+%Y-%m-%d')'", "access_level": 30 }' | glab api projects/${GITLAB_PROJECT}/access_tokens -X POST --header "Content-Type:application/json" --input - | yq e .token)
   export GITLAB_USER=$(yq e '.hosts."gitlab.com".user' ~/.config/glab-cli/config.yml)
   ```
1. Open this folder in VSCode
   ```sh
   cd FreeRTLS/gitlab_runner
   code .
   ```
1. Click in "_Open in container_"
1. Initialize a new or existing Terraform working directory
   ```sh
   TERRAFROM_STATE=gitlab_runner
   terraform init -reconfigure \
       -backend-config="address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT/terraform/state/$TERRAFROM_STATE" \
       -backend-config="lock_address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT/terraform/state/$TERRAFROM_STATE/lock" \
       -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT/terraform/state/$TERRAFROM_STATE/lock" \
       -backend-config="username=${GITLAB_USER}" \
       -backend-config="password=${GITLAB_TOKEN}" \
       -backend-config="lock_method=POST" \
       -backend-config="unlock_method=DELETE" \
       -backend-config="retry_wait_min=5"
   ```

    Use the instruction listed [here](
    https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html#get-started-using-local-development).
