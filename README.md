# FreeRTLS
## Description

FreeRTLS is an indoor location system that uses UWB technology to provide
real-time location information. With this system, users can track the location
of assets or people within a building or indoor environment with high precision.

The system is built on AWS, using AWS IoT Core for device communication and data
management. The UWB sensors are deployed throughout the indoor space and
transmit location data to AWS IoT Core, where it is processed and analyzed. The
system includes a web-based dashboard for users to view the location data in
real-time, as well as APIs for integrating the data with other systems.

## Features

- Real-time location tracking: Track the location of assets or people within an
  indoor space with high precision.
- AWS hosting: The system is hosted in AWS, providing scalability, reliability,
  and security.
- UWB technology: UWB sensors are used to provide precise location data.
- AWS IoT Core: AWS IoT Core is used for device communication and data
  management.
- Web-based dashboard: A user-friendly dashboard is provided for viewing
  location data in real-time.
- APIs: APIs are provided for integrating the data with other systems.

## Benefits

- Accurate and reliable location tracking: With UWB technology and AWS hosting,
  the system provides accurate and reliable location data.
- Scalability: The system can easily be scaled to handle large indoor spaces or
  multiple locations.
- Integration: The APIs allow the location data to be integrated with other
  systems, such as asset management or security systems.
- Real-time monitoring: The web-based dashboard allows users to monitor location
  data in real-time, enabling quick response to any issues or incidents.

## Technologies Used

- UWB technology for location tracking
- AWS for hosting and IoT Core for device communication and data management
- Web technologies such as HTML, CSS, and JavaScript for the dashboard

## Future Development

- Mobile app: A mobile app could be developed to provide users with location
  data on-the-go.
- Machine learning: Machine learning algorithms could be used to improve the
  accuracy and reliability of the location data.
- Integration with other AWS services: The system could be integrated with other
  AWS services, such as Lambda or SNS, to enable more advanced functionality.

## Getting Started
### Prerequisites
- Docker: This will be used to run any necessary containers for the project.
- VSCode: This is the recommended IDE for working with the project.

### Opening Code Folders

To work on specific features within the project, you'll need to open each code
folder in a separate instance of your IDE. Here's how to do that with VSCode:

1. Clone the repository: `git clone https://gitlab.com/FreeRTLS.git`
2. Navigate to the root of the repository: `cd FreeRTLS`
3. Open each code folder in a separate VSCode window: `code folder-name`

Replace `folder-name` with the name of the folder you want to open. Repeat step
3 for each code folder you want to work on.

## License

[LICENSE](LICENSE)

## Contact

For more information about this project, please contact us at
davidmbroin[at]gmail.com
